export const ADD_TODO = 'ADD_TODOS';
export const EDIT_TODO = 'EDIT_TODOS';
export const IS_EDITING = 'IS_EDITING';
export const DELETE_TODO = 'DELETE_TODO';
export const EXECUTE_SEARCH = 'EXECUTE_SEARCH';
export const SET_DONE_STATUS = 'SET_DONE_STATUS';