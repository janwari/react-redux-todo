import { ADD_TODO, EDIT_TODO, DELETE_TODO, IS_EDITING, EXECUTE_SEARCH, SET_DONE_STATUS } from './ActionTypes';

export const deleteTodo = id => dispatch => {
  dispatch({
    type: DELETE_TODO,
    id
  });
}

export const addTodo = (quadrant, text) => dispatch => {
  dispatch({
    type: ADD_TODO,
    quadrant,
    text
  });
}

export const editTodo = (id, text) => dispatch => {
  dispatch({
    type: EDIT_TODO,
    id,
    text
  });
}

export const setDoneStatus = (id) => dispatch => {
  dispatch({
    type: SET_DONE_STATUS,
    id
  });
}

export const startEditing = (id) => dispatch => {
  dispatch({
    type: IS_EDITING,
    id
  });
}

export const executeSearch = query => dispatch => {
  dispatch({
    type: EXECUTE_SEARCH,
    query
  });
}