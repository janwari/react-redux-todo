import React, { Component } from 'react'
import TodoItem from '../containers/TodoItem'

export default class TodoList extends Component {
  render() {
    const todoItems = this.props.todos.map((todo, index) =>
      <TodoItem
        key={index.toString()}
        todo={todo}
        quadrant={this.props.quadrant}
      />
    );
    return (
      <ul>
        {todoItems}
      </ul>
    )
  }
}
