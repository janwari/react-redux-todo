import React, { Component } from 'react'
import { connect } from 'react-redux';
import { executeSearch } from '../../actions/TodoActionCreators'

class SearchForm extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.executeSearch(event.target.value)
  }
  
  render() {
    return (
      <div>
          <input 
            name="search" 
            type="text" 
            className="search-todos"
            placeholder="Search all Todo items" 
            onChange={this.handleChange}
          />
      </div>
    )
  }
}

export default connect(null, { executeSearch })(SearchForm);
