import React, { Component } from 'react'
import { connect } from 'react-redux';
import { editTodo } from '../../actions/TodoActionCreators'

class EditTodoForm extends Component {
  static ESCAPE_KEY = 27;

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEscape = this.handleEscape.bind(this);
    this.editTodoInput = React.createRef();
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleEscape, false);
    this.editTodoInput.current.value = this.props.todo.text;
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleEscape, false);
  }

  handleEscape(event) {
    if (event.keyCode && event.keyCode === EditTodoForm.ESCAPE_KEY) {
      this.props.editTodo(this.props.todo.id,
        this.props.todo.text);
      this.editTodoInput.current.value = this.props.todo.text;
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    if (!this.props.isEditing.status) return;
    this.props.editTodo(this.props.todo.id,
      this.editTodoInput.current.value);
  }

  render() {
    return (
      <form
        onSubmit={this.handleSubmit}
        className={this.props.className}>

        <input
          name="edittodo"
          type="text"
          className="edit-todo"
          onBlur={this.handleSubmit}
          ref={this.editTodoInput}
        />
      </form>
    )
  }
}

const mapStateToProps = state => ({
  isEditing: state.todos.isEditing
});


export default connect(mapStateToProps, { editTodo })(EditTodoForm);
