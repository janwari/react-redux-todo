import React, { Component } from 'react'
import { connect } from 'react-redux';
import { deleteTodo, startEditing, setDoneStatus } from '../../actions/TodoActionCreators'
import EditTodoForm from './EditTodoForm';

class TodoItem extends Component {

  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleDone = this.handleDone.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleDelete() {
    this.props.deleteTodo(this.props.todo.id);
  }

  handleDone() {
    this.props.setDoneStatus(this.props.todo.id);
  }

  handleEdit() {
    this.props.startEditing(this.props.todo.id);
  }

  render() {
    const doneClassName = this.props.todo.done ? 'done-true' : 'done-false';
    const editingClassName = (this.props.isEditing.status
      && this.props.isEditing.id === this.props.todo.id)
      ? 'editing-true' : 'editing-false';
    const classNames = [doneClassName, editingClassName].join(' ');

    return (
      <li>
        <span className={classNames}>
          <input
            type="checkbox"
            className="todo-done"
            checked={this.props.todo.done}
            onChange={this.handleDone}
          />
          <span
            className={doneClassName}
            onDoubleClick={this.handleEdit}>
            {this.props.todo.text}
          </span>
          <button onClick={this.handleDelete}>✖</button>
        </span>

        <EditTodoForm
          className={editingClassName}
          todo={this.props.todo}
          quadrant={this.props.value}
        />
      </li>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos.items,
  isEditing: state.todos.isEditing
});

export default connect(mapStateToProps,
  { deleteTodo, setDoneStatus, startEditing })(TodoItem);