import React, { Component } from 'react'
import { connect } from 'react-redux';
import CoveyQuadrants from "../presentational/CoveyQuadrants";
import { filterTodos } from '../../utils'


class MainContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <CoveyQuadrants todos={this.props.todos} />
      </React.Fragment>
    )
  }
}

// Note: Its important to ensure that the
// value of the state property (i.e. state.todos) matches
// exactly to what was defined in the root reducer.
const mapStateToProps = state => ({
  todos: filterTodos(state.todos.items, state.search.query)
});


export default connect(mapStateToProps, null)(MainContainer);
