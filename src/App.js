import React, { Component } from 'react';
import './App.css';
import MainContainer from "./components/containers/MainContainer";
import { Provider } from 'react-redux';
import store from "./store";
import Footer from './components/presentational/Footer';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <h1 className="App-title">Covey’s Time Management Quadrants</h1>
          <MainContainer />
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
