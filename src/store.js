import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'
import { loadState, saveState } from './localStorage';

const initialState = loadState();

const middleware = [thunk];

const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

// To save all actions to localStorage
store.subscribe(() => {
  saveState({
    todos: store.getState().todos
  })
})

export default store;