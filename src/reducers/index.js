import { combineReducers } from 'redux';
import TodoReducer from './TodoReducer';
import SearchReducer from './SearchReducer';

export default combineReducers({
  todos: TodoReducer,
  search: SearchReducer
});
