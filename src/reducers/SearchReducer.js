import { EXECUTE_SEARCH } from '../actions/ActionTypes';


const initialState = {
  query: ''
}

const SearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case EXECUTE_SEARCH:
      return {
        query: action.query
      }
    default:
      return state;
  }
}
export default SearchReducer;