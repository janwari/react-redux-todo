import { ADD_TODO, EDIT_TODO, DELETE_TODO, IS_EDITING, SET_DONE_STATUS } from '../actions/ActionTypes';


const initialState = {
  items: [],
  isEditing: {
    status: false,
    id: ''
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        items: [...state.items, {
          // Poorman's ways of assining ID. 
          // Since information is persisted in localstorage this works for now.
          id: Date.now(),
          text: action.text,
          quadrant: action.quadrant
        }]
      };
    case SET_DONE_STATUS:
      return {
        ...state,
        items: state.items.map((todo) => {
          if (todo.id === action.id) {
            todo.done = !todo.done;
          }
          return todo;
        })
      };
    case EDIT_TODO:
      return {
        ...state,
        items: state.items.map((todo) => {
          if (todo.id === action.id) {
            todo.text = action.text;
          }
          return todo;
        }),
        isEditing: {
          status: false,
          id: ''
        }
      };
    case DELETE_TODO:
      return {
        ...state,
        items: state.items.filter((todo) => todo.id !== action.id)
      }
    case IS_EDITING:
      return {
        ...state,
        isEditing: {
          status: true,
          id: action.id
        }
      }
    default:
      return state;
  }
}