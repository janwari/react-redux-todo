// Copied from 
// https://adrianarlett.gitbooks.io/idiomatic-redux-by-dan-abramov/content/persisting-the-state-to-the-local-storage.html
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('react-covery-todo');
    if (serializedState === null) {

      // Initial dummy todos
      return {
        todos: {
          items: [
            { "text": "Learn React", "done": true, id: 1534704902887, quadrant: 1 },
            { "text": "Then learn Redux", "done": true, id: 1534704902287, quadrant: 1 },
            { "text": "Learn MobX. (To edit double click me)", "done": false, id: 1534704302287, quadrant: 2 },
            { "text": "Create sample app", "done": false, id: 1534702302287, quadrant: 2 },
            { "text": "Print report", "done": false, id: 1534202302287, quadrant: 3 },
            { "text": "Research time management", "done": false, id: 1534202302187, quadrant: 3 },
            { "text": "Call aunt May", "done": false, id: 1534202102187, quadrant: 4 },
            { "text": "Pickup things from groceries", "done": false, id: 1534202102189, quadrant: 4 }
          ],
          isEditing: {
            status: false,
            id: ''
          }
        }
      }
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('react-covery-todo', serializedState);
  } catch (err) {
    // Ignore write errors.
  }
};